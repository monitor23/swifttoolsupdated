package ObjectRepositories;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AGIPage {
	WebDriver driver;
	WebDriverWait wait;

	@SuppressWarnings("deprecation")
	public AGIPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 300);
		PageFactory.initElements(driver, this);
	}

	public void SendAGIMessage() throws InterruptedException {
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("j_idt36:qmgrName_label"))).click();
		for (int i = 1; i <= 20; i++) {
			Thread.sleep(1000);
			String qManager = wait
					.until(ExpectedConditions.visibilityOfElementLocated(By.id("j_idt36:qmgrName_" + i + "")))
					.getText();
			if (qManager.equals("AGIQM1")) {
				System.out.println("AGI Message Details");
				System.out.println("--------------------------");
				System.out.println("Queue Manager : " + qManager);
				driver.findElement(By.id("j_idt36:qmgrName_" + i + "")).click();
				break;
			} else {
				continue;
			}

		}
		Thread.sleep(6000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("j_idt36:inQueue_label"))).click();
		for (int j = 1; j <= 20; j++) {
			String inputQueue = wait
					.until(ExpectedConditions.visibilityOfElementLocated(By.id("j_idt36:inQueue_" + j + ""))).getText();
			if (inputQueue.equals("PART.EQ")) {
				System.out.println("Input Queue : " + inputQueue);
				Thread.sleep(1500);
				driver.findElement(By.id("j_idt36:inQueue_" + j + "")).click();
				break;
			} else {
				continue;
			}
		}
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("span.ui-button-text:nth-child(1)")))
				.click();
		String info = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".ui-messages-info-detail")))
				.getText();
		System.out.println("Message Status : " + info);
		Thread.sleep(1000);
		driver.close();

	}

	public void VerifyAGIMessage() throws InterruptedException {
		Thread.sleep(1500);
		for (int i = 1; i <= 20; i++) {
			Thread.sleep(1000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("j_idt36:qmgrName"))).click();
			String qManager = wait
					.until(ExpectedConditions.visibilityOfElementLocated(By.id("j_idt36:qmgrName_" + i + "")))
					.getText();
			if (qManager.equals("AGIQM1")) {
				driver.findElement(By.id("j_idt36:qmgrName_" + i + "")).click();
				break;
			} else {
				continue;
			}
		}
		Thread.sleep(6000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("j_idt36:inQueue_label"))).click();
		for (int j = 1; j <= 20; j++) {
			String inputQueue = wait
					.until(ExpectedConditions.visibilityOfElementLocated(By.id("j_idt36:inQueue_" + j + ""))).getText();
			if (inputQueue.equals("TIPS.RQ")) {
				Thread.sleep(1500);
				driver.findElement(By.id("j_idt36:inQueue_" + j + "")).click();
				break;
			} else {
				continue;
			}
		}
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("span.ui-button-text:nth-child(1)")))
				.click();
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.cssSelector("tr.ui-widget-content > td:nth-child(1)")));
		String quantity = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.id("formSalida:resultsTable:0:messages")))
				.getText();
		System.out.println("No.of messages available : " + quantity);
		Thread.sleep(2000);
		driver.close();
	}

}
