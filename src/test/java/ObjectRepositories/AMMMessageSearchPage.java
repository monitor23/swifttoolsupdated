package ObjectRepositories;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AMMMessageSearchPage {
	WebDriver driver;
	WebDriverWait wait;

	@SuppressWarnings("deprecation")
	public AMMMessageSearchPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 300);
		PageFactory.initElements(driver, this);
	}

	public void SearchAMMMessage() throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("gwt-debug-Alliance_Access_Entry-frame")));
		driver.switchTo().frame("gwt-debug-Alliance_Access_Entry-frame");
		Thread.sleep(6000);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("gwt-debug--messenger-messageSearch-criteria-actionSearch"))).click();
		Thread.sleep(6000);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("gwt-debug--messenger-messageSearch-criteria-actionSearch"))).click();
		Thread.sleep(2000);
		driver.switchTo().defaultContent();
		Thread.sleep(4000);
	}

}
