package ObjectRepositories;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SwiftToolsHomePage {
	WebDriver driver;
	WebDriverWait wait;

	@SuppressWarnings("deprecation")
	public SwiftToolsHomePage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 300);
		PageFactory.initElements(driver, this);
	}

	public void ClickSoapha() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.cssSelector("ul.ui-helper-reset > li:nth-child(1)")));
		Actions a = new Actions(driver);
		a.moveToElement(driver.findElement(By.cssSelector("ul.ui-helper-reset > li:nth-child(1)"))).build().perform();
		Thread.sleep(1500);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.cssSelector("li.ui-widget:nth-child(5) > a:nth-child(1)")));
		a.moveToElement(driver.findElement(By.cssSelector("li.ui-widget:nth-child(5) > a:nth-child(1)"))).build()
				.perform();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.cssSelector("li.ui-widget:nth-child(5) > ul:nth-child(2) > li:nth-child(1)"))).click();
	}

	public void ClickMT199() throws InterruptedException {

		Thread.sleep(1000);
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.cssSelector("ul.ui-helper-reset > li:nth-child(1)")));
		Actions a = new Actions(driver);
		a.moveToElement(driver.findElement(By.cssSelector("ul.ui-helper-reset > li:nth-child(1)"))).build().perform();
		Thread.sleep(1500);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.cssSelector("li.ui-widget:nth-child(5) > a:nth-child(1)")));
		a.moveToElement(driver.findElement(By.cssSelector("li.ui-widget:nth-child(5) > a:nth-child(1)"))).build()
				.perform();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.cssSelector("li.ui-widget:nth-child(5) > ul:nth-child(2) > li:nth-child(2)"))).click();

	}

	public void ClickAGI() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.cssSelector("ul.ui-helper-reset > li:nth-child(1)")));
		Actions a = new Actions(driver);
		a.moveToElement(driver.findElement(By.cssSelector("ul.ui-helper-reset > li:nth-child(1)"))).build().perform();
		Thread.sleep(1500);

		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.cssSelector(".ui-menuitem-active > ul:nth-child(2) > li:nth-child(2) > a:nth-child(1)"))).click();

	}

	public void ClickBrowseQueue() throws InterruptedException {
		Thread.sleep(1500);
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.cssSelector("ul.ui-helper-reset > li:nth-child(1)")));
		Actions a = new Actions(driver);
		a.moveToElement(driver.findElement(By.cssSelector("ul.ui-helper-reset > li:nth-child(1)"))).build().perform();
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("li.ui-widget:nth-child(4)")));
		a.moveToElement(driver.findElement(By.cssSelector("li.ui-widget:nth-child(4)"))).build().perform();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.cssSelector("li.ui-widget:nth-child(4) > ul:nth-child(2) > li:nth-child(2)"))).click();
	}

}
