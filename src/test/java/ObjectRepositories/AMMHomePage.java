package ObjectRepositories;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AMMHomePage {
	WebDriver driver;
	WebDriverWait wait;

	@SuppressWarnings("deprecation")
	public AMMHomePage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 30);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "gwt-debug-desktop-applications-com.swift.Access.samSearch")
	WebElement MsgSearch;

	public WebElement MsgSearch() {
		wait.until(ExpectedConditions.visibilityOf(MsgSearch));
		return MsgSearch;
	}

	public void Logout() throws InterruptedException {
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.cssSelector("#gwt-debug-desktop-topmenu-Logout > div:nth-child(2) > div:nth-child(1)"))).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("gwt-debug-dialog-ask-0-ok"))).click();
		Thread.sleep(2500);
		driver.close();
	}

}
