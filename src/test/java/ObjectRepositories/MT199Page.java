package ObjectRepositories;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MT199Page {
	WebDriver driver;
	WebDriverWait wait;
	SimpleDateFormat date = new SimpleDateFormat("hhmmssss", Locale.ENGLISH);
	String currentDate = date.format(new Date());
	String ref = "MT199-" + currentDate;
	String mRef = "MESSAGE TEXT" + currentDate;

	@SuppressWarnings("deprecation")
	public MT199Page(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 300);
		PageFactory.initElements(driver, this);
	}

	public void SendMT199Message() throws InterruptedException {

		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("j_idt36:block4_field20"))).clear();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("j_idt36:block4_field20"))).sendKeys(ref);
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("j_idt36:block4_field79"))).clear();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("j_idt36:block4_field79"))).sendKeys(mRef);
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("j_idt36:qmgrName_label"))).click();
		for (int i = 1; i <= 10; i++) {
			Thread.sleep(1000);
			String qManager = wait
					.until(ExpectedConditions.visibilityOfElementLocated(By.id("j_idt36:qmgrName_" + i + "")))
					.getText();
			if (qManager.equals("QM_NETLINK")) {
				System.out.println("Queue Manager : " + qManager);
				driver.findElement(By.id("j_idt36:qmgrName_" + i + "")).click();
				break;
			} else {
				continue;
			}
		}
		Thread.sleep(5000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("j_idt36:inQueue_label"))).click();
		for (int j = 1; j <= 30; j++) {
			String inputQueue = wait
					.until(ExpectedConditions.visibilityOfElementLocated(By.id("j_idt36:inQueue_" + j + ""))).getText();
			if (inputQueue.equals("Q_TOSWIFT")) {
				System.out.println("Input Queue : " + inputQueue);
				Thread.sleep(1500);
				driver.findElement(By.id("j_idt36:inQueue_" + j + "")).click();
				break;
			} else {
				continue;
			}
		}

		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("span.ui-button-text:nth-child(1)")))
				.click();
		String info = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".ui-messages-info-detail")))
				.getText();
		System.out.println("Message Status : " + info);
		Thread.sleep(2000);
		driver.close();

	}

}
