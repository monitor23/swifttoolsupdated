package TestCases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ObjectRepositories.AGIPage;
import ObjectRepositories.SwiftToolsHomePage;

public class SwiftTools_VerifyAGI {
	WebDriver driver = null;

	@BeforeTest
	public void Initialization() {
		System.setProperty("webdriver.gecko.driver", "C:\\MyWorkspace\\geckodriver.exe");
		System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE, "true");
		System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, "C:\\SeleniumLogs\\seleniumlogs.txt");
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("http://192.168.5.40:8085/swifttools/index.xhtml");
	}

	@Test
	public void VerifyAGIMessage() throws InterruptedException {
		SwiftToolsHomePage sth = new SwiftToolsHomePage(driver);
		sth.ClickBrowseQueue();
		AGIPage ap = new AGIPage(driver);
		ap.VerifyAGIMessage();
	}

}
