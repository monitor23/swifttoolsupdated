package TestCases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ObjectRepositories.MT199Page;
import ObjectRepositories.SwiftToolsHomePage;

public class SwiftTools_MT199 {
	WebDriver driver = null;

	@BeforeTest
	public void Initialization() {

		System.setProperty("webdriver.gecko.driver", "C:\\MyWorkspace\\geckodriver.exe");
		System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE, "true");
		System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, "C:\\SeleniumLogs\\seleniumlogs.txt");
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("http://192.168.5.40:8085/swifttools/index.xhtml");
	}

	@Test
	public void MessageSend() throws InterruptedException {

		SwiftToolsHomePage sth = new SwiftToolsHomePage(driver);
		sth.ClickMT199();
		MT199Page mtp = new MT199Page(driver);
		mtp.SendMT199Message();

	}

}
