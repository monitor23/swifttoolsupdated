package TestCases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ObjectRepositories.AMMHomePage;
import ObjectRepositories.AMMLoginPage;
import ObjectRepositories.AMMMessageSearchPage;

public class AMM_SearchMessage {
	WebDriver driver = null;

	@BeforeTest
	public void Initializtion() throws InterruptedException {
		System.setProperty("webdriver.gecko.driver", "C:\\MyWorkspace\\geckodriver.exe");
		System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE, "true");
		System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, "C:\\SeleniumLogs\\seleniumlogs.txt");
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("https://netlink04.netlink-testlabs.com/swp/group/messagemgmt/#");
		AMMLoginPage alp = new AMMLoginPage(driver);
		alp.LoginOption().click();
		alp.WaitFunction();
		alp.Username().sendKeys("seleniumamm");
		alp.Password().sendKeys("June2021June2021+");
		alp.Login().click();

	}

	@Test
	public void MessageSearch() throws InterruptedException {

		AMMHomePage ahp = new AMMHomePage(driver);
		ahp.MsgSearch().click();
		AMMMessageSearchPage amsp = new AMMMessageSearchPage(driver);
		amsp.SearchAMMMessage();
		ahp.Logout();
	}

}
